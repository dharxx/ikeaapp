//
//  ViewController.m
//  IKEAApplication
//
//  Created by Dhanu Saksrisathaporn on 1/20/2559 BE.
//  Copyright © 2559 Dhanu Saksrisathaporn. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "DataManager.h"
#import "SphereViewController.h"
#import "NSObject+WDIOS.h"

typedef enum : NSUInteger {
    QRStateNone,
    QRStateScanning,
    QRStateLoadingData,
    QRStateSuccess,
    QRStateFail,
} QRState;

@interface ViewController ()<AVCaptureMetadataOutputObjectsDelegate>
{
    QRState qrState;
    
}

@property (nonatomic) NSDictionary *testDatas;

@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;

@property IBOutlet UILabel *labelLog;
@property IBOutlet UIView *viewPreview;
- (IBAction)tapScan:(id)sender;
@end

@implementation ViewController


//

- (IBAction)tapScan:(id)sender
{
    if (qrState == QRStateNone || qrState == QRStateFail) {
        
        if ([self startReading]) {
            [self setState:QRStateScanning];
        }
    }
}
//
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.testDatas = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"testDatas" ofType:@"plist"]];
    
    [self setState:QRStateNone];
    // Do any additional setup after loading the view, typically from a nib.
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    [_viewPreview becomeFirstResponder];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self stopReading];
    [self setState:QRStateNone];
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
//    self.view = nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)startReading {
    NSError *error;
    
    // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
    // as the media type parameter.
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    // Get an instance of the AVCaptureDeviceInput class using the previous device object.
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    
    if (!input) {
        // If any error occurs, simply log the description of it and don't continue any more.
        NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
    
    // Initialize the captureSession object.
    self.captureSession = [[AVCaptureSession alloc] init];
    // Set the input device on the capture session.
    [_captureSession addInput:input];
    
    
    // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [_captureSession addOutput:captureMetadataOutput];
    
    // Create a new serial dispatch queue.
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
    
    // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
    self.videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [_videoPreviewLayer setFrame:_viewPreview.layer.bounds];
    [_viewPreview.layer addSublayer:_videoPreviewLayer];
    
    
    // Start video capture.
    [_captureSession startRunning];
    
    return YES;
}


-(void)stopReading{
    
    // Stop video capture and make the capture session object nil.
    [_captureSession stopRunning];
    self.captureSession = nil;
    
    // Remove the video preview layer from the viewPreview view's layer.
    [_videoPreviewLayer removeFromSuperlayer];
}
//default scaning load fail
- (void)setState:(QRState)state
{
    [self setState:state text:nil];
}
- (void)setState:(QRState)state text:(NSString *)forceText;
{
    NSString *text;
    UIColor *textColor;
    
    if (state == QRStateNone) {
        text = @"TAP TO START SCAN.";
        textColor = [UIColor whiteColor];
    }
    else if (state == QRStateScanning) {
        text = @"SCANNING FOR QR CODE.";
        textColor = [UIColor blueColor];
    }
    else if (state == QRStateLoadingData) {
        text = @"LOAD DATA...";
        textColor = [UIColor yellowColor];
    }
    else if (state == QRStateSuccess) {
        text = @"SUCCESSFULLY SCANNED. LOADING VIEW...";
        textColor = [UIColor greenColor];
    }
    else if (state == QRStateFail) {
        text = @"INVALID QR CODE. TAP TO START SCAN AGAIN.";
        textColor = [UIColor redColor];
    }
    else {
        return;
    }
    if (forceText) {
        text = forceText;
    }
    qrState = state;
    [_labelLog performSelectorOnMainThread:@selector(setText:) withObject:text waitUntilDone:NO];
    [_labelLog performSelectorOnMainThread:@selector(setTextColor:) withObject:textColor waitUntilDone:NO];
}


- (void)loadNextView:(NSString *)code
{
    
    [self performSegueWithIdentifier:@"scan" sender:code];
    
}

- (void)testLoad:(NSArray *)codes
{
    NSMutableArray *objects = [NSMutableArray arrayWithCapacity:codes.count];
    for (NSString *code in codes) {
        NSMutableDictionary *mData = [_testDatas[code] mutableCopy];
        if (mData) {
            mData[@"id"] = code;
            [objects addObject:mData];
        }
    }
    NSDictionary *selectedData = nil;
 
    if (objects.count > 1) {
        for (NSDictionary *obj in objects) {
            NSString *code = obj[@"id"];
            if (![[DataManager mainObject] hasObject:code]) {
                selectedData = obj;
                break;
            }
        }
    }
    if (!selectedData && objects.count >= 1) {
        selectedData = objects[0];
    }
    
    if (selectedData) {
        NSString *code = selectedData[@"id"];
        [self setState:QRStateSuccess];
        [[DataManager mainObject] addObject:selectedData forKey:code];
        [self performSelector:@selector(loadNextView:) withObject:code afterDelay:0.5f];
    }
    else {
        [self setState:QRStateFail];
    }

}
- (void)loadData:(NSString *)code
{
    //load data
    //[self testLoad:code];
    
    [self performSelector:@selector(testLoad:) withObject:code afterDelay:1.0f];
    //

}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [(SphereViewController *)segue.destinationViewController setCode:sender];
}
#pragma mark - AVCaptureMetadataOutputObjectsDelegate method implementation

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    
    // Check if the metadataObjects array is not nil and it contains at least one object.
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        // Get the metadata object.
        
        NSMutableArray *codes = [NSMutableArray arrayWithCapacity:metadataObjects.count];
        for (AVMetadataMachineReadableCodeObject *metadataObj in metadataObjects) {
            if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
                [codes addObject:metadataObj.stringValue];
            }
        }
        
        [self stopReading];
        [self setState:QRStateLoadingData];
        [self performSelectorOnMainThread:@selector(loadData:) withObject:codes waitUntilDone:NO];
    }
    
    
}

@end
