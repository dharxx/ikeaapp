//
//  SphereViewController.m
//  IKEAApplication
//
//  Created by Dhanu Saksrisathaporn on 1/20/2559 BE.
//  Copyright © 2559 Dhanu Saksrisathaporn. All rights reserved.
//

#import "SphereViewController.h"
#import <SpriteKit/SpriteKit.h>
#import <SceneKit/SceneKit.h>
#import <CoreMotion/CoreMotion.h>
#import "DataManager.h"
#import "NSObject+WDIOS.h"

#define SENSOR_ORIENTATION [[UIApplication sharedApplication] statusBarOrientation]

#define FPS_DEFAULT 30.0f
#define ONE_FPS_DEFAULT (1/FPS_DEFAULT)

@interface SphereViewController ()
{
    CMMotionManager *motionManager;
    SCNNode *camera;
    SCNNode *cameraBase;
    SCNNode *cameraLegForRotX;
    SCNNode *cameraLegForRotY;
    SCNNode *sphere;
    
    IBOutlet SCNView *sceneView;
    SCNScene *scene;
    
    float lookAzimuth;
    float lookAltitude;
    
    SCNMatrix4 attitudeMatrix;
    SCNVector3 lookVector;
    
    CGPoint originalPoint;
}
@property NSDictionary *data;
@end

@implementation SphereViewController
SCNMatrix4 SCNMatrix4Make(float m00, float m01, float m02, float m03,
                          float m10, float m11, float m12, float m13,
                          float m20, float m21, float m22, float m23,
                          float m30, float m31, float m32, float m33)
{
    SCNMatrix4 m = { m00, m01, m02, m03,
        m10, m11, m12, m13,
        m20, m21, m22, m23,
        m30, m31, m32, m33 };
    return m;
}

-(SCNMatrix4) getDeviceOrientationMatrix{
    CMRotationMatrix a = [[[motionManager deviceMotion] attitude] rotationMatrix];
    // arrangements of mappings of sensor axis to virtual axis (columns)
    // and combinations of 90 degree rotations (rows)
    
    //    NSLog(@"\n\n\n\n\n\n\n\n\n\n");
    //    NSLog(@"%.2f %.2f %.2f",a.m11,a.m12,a.m13);
    //    NSLog(@"%.2f %.2f %.2f",a.m21,a.m22,a.m23);
    //    NSLog(@"%.2f %.2f %.2f",a.m31,a.m32,a.m33);
//
    
    
    if(SENSOR_ORIENTATION == UIInterfaceOrientationLandscapeRight){
        return SCNMatrix4Make( a.m21, a.m23, -a.m22, 0.0f,
                              -a.m11, -a.m13, a.m12, 0.0f,
                              a.m31, a.m33, -a.m32, 0.0f,
                              0.0f , 0.0f , 0.0f , 1.0f);
    }
    if(SENSOR_ORIENTATION == UIInterfaceOrientationLandscapeLeft){
        return SCNMatrix4Make( -a.m21, -a.m23, a.m22, 0.0f,
                              a.m11, a.m13, -a.m12, 0.0f,
                              a.m31, a.m33, -a.m32, 0.0f,
                              0.0f , 0.0f , 0.0f , 1.0f);
    }
    if(SENSOR_ORIENTATION == UIInterfaceOrientationPortraitUpsideDown){
        return SCNMatrix4Make( -a.m11, -a.m13, a.m12, 0.0f,
                              -a.m21, -a.m23, a.m22, 0.0f,
                              a.m31, a.m33, -a.m32, 0.0f,
                              0.0f , 0.0f , 0.0f , 1.0f);
    }
    return SCNMatrix4Make( a.m11, a.m13, -a.m12, 0.0f,
                          a.m21, a.m23, -a.m22, 0.0f,
                          a.m31, a.m33, -a.m32, 0.0f,
                          0.0f , 0.0f , 0.0f , 1.0f);
}

- (void)updateCamera
{
    attitudeMatrix = [self getDeviceOrientationMatrix];
//    lookVector = SCNVector3Make(-attitudeMatrix.m12,
//                                 -attitudeMatrix.m22,
//                                 -attitudeMatrix.m32);
//    lookAzimuth = atan2f(lookVector.x, lookVector.z);
//    lookAltitude = asinf(lookVector.y);
    
//    CMQuaternion a = [[[motionManager deviceMotion] attitude] quaternion];
//    cameraBase.rotation = SCNVector4Make(a.x, a.y, a.z, a.w);
    
    cameraBase.transform = attitudeMatrix;
}

- (void)panHandle:(UIPanGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateBegan) {
        originalPoint = [gesture locationInView:sceneView];
        
//        [cameraLegForRotX removeAllActions];
//        [cameraLegForRotY removeAllActions];
        
    }
    else if (gesture.state == UIGestureRecognizerStateChanged) {
        CGPoint pointOffset = [gesture locationInView:sceneView];
        pointOffset.x -= originalPoint.x;
        pointOffset.y -= originalPoint.y;

        
        CGSize screenSize = sceneView.bounds.size;
        float screenPanRatio = 0.3 * M_PI / screenSize.width;
        
        pointOffset.x *= screenPanRatio;
        pointOffset.y *= screenPanRatio;
        
        cameraLegForRotX.eulerAngles = SCNVector3Make(pointOffset.y,0, 0);
        cameraLegForRotY.eulerAngles = SCNVector3Make(0,pointOffset.x, 0);
//        cameraLegForRotX.rotation = SCNVector4Make(1, 0, 0, pointOffset.y);
//        cameraLegForRotY.rotation = SCNVector4Make(0, 1, 0, pointOffset.x);
    }
    else if (gesture.state == UIGestureRecognizerStateEnded) {
        
        SCNAction *actionX = [SCNAction rotateToX:0 y:0 z:0 duration:0.3];
        SCNAction *actionY = [SCNAction rotateToX:0 y:0 z:0 duration:0.3];
        [gesture setEnabled:NO];
        [cameraLegForRotX runAction:actionX forKey:@"backX"];
        [cameraLegForRotY runAction:actionY forKey:@"backY" completionHandler:^{
            [gesture setEnabled:YES];
        }];
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];

    
    scene = [SCNScene sceneNamed:@"SphereScene.scn"];
    sceneView.scene = scene;
    
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panHandle:)];
    pan.maximumNumberOfTouches = 1;
    [sceneView addGestureRecognizer:pan];
    // create and add a camera to the scene
    camera = [scene.rootNode childNodeWithName:@"camera" recursively:YES];
    cameraBase = [scene.rootNode childNodeWithName:@"cameraBase" recursively:YES];
    cameraLegForRotX = [scene.rootNode childNodeWithName:@"cameraLegForRotX" recursively:YES];
    cameraLegForRotY = [scene.rootNode childNodeWithName:@"cameraLegForRotY" recursively:YES];
    sphere = [scene.rootNode childNodeWithName:@"sphere" recursively:YES];
    sphere.hidden = YES;
    
    if (_code) {
        self.data = [[DataManager mainObject] datasDict][_code];
        [self.navigationItem setTitle:_data[@"name"]];
        sphere.geometry.firstMaterial.diffuse.contents = [UIImage imageNamed:_code];
    }
    
    SCNMatrix4 transform = SCNMatrix4Identity;
    transform = SCNMatrix4Scale(transform, -1, 1, 1);
    transform = SCNMatrix4Translate(transform, 1, 0, 0);
    sphere.geometry.firstMaterial.diffuse.contentsTransform = transform;
    
    motionManager = [[CMMotionManager alloc] init];
    [motionManager setAccelerometerUpdateInterval:ONE_FPS_DEFAULT];
    [motionManager startDeviceMotionUpdatesToQueue:[NSOperationQueue currentQueue]
                                       withHandler:^(CMDeviceMotion *motion, NSError *error) {
                                           // Handle motion and update UI here.
                                           if(sphere.hidden) {
                                               sphere.hidden = NO;
                                               sphere.opacity = 0;
                                               SCNAction *a = [SCNAction fadeInWithDuration:0.3];
                                               [sphere runAction:a];
                                           }
                                           [self updateCamera];
                                       }];
    [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(timer:) userInfo:nil repeats:YES];
    
}

- (void)timer:(NSTimer *)timer
{
    NSLog(@"%@ %@",@(lookAltitude),@(lookAzimuth));
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
