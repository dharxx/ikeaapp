//
//  DataManager.m
//  IKEAApplication
//
//  Created by Dhanu Saksrisathaporn on 1/20/2559 BE.
//  Copyright © 2559 Dhanu Saksrisathaporn. All rights reserved.
//

#import "DataManager.h"

@interface DataManager ()
@property (nonatomic,retain) NSMutableDictionary *mDict;
@property (copy,nonatomic) NSString *path;
@end

@implementation DataManager


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.path = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0] stringByAppendingPathComponent:@"datas"];
        
        self.mDict = [NSMutableDictionary dictionaryWithContentsOfFile:_path];
        if (!_mDict) {
            self.mDict = [NSMutableDictionary dictionaryWithCapacity:10];
        }
    }
    return self;
}
- (NSDictionary *)datasDict
{
    return [_mDict copy];
}
- (NSArray *)datas
{
    
    return [[_mDict allValues] copy];
}
- (BOOL)hasObject:(NSString *)key
{
    return _mDict[key] != nil;
}
//YES if successfully added
//NO if fail
//if already has object old one will disappear;
- (void)updateData
{
    [_mDict writeToFile:_path atomically:YES];
}
- (BOOL)addObject:(id)data forKey:(NSString *)key
{
    if (data && key) {
        _mDict[key] = data;
        [self updateData];
        return YES;
    }
    return NO;
}

//return removed object
- (id)removeObject:(NSString *)key
{
    if (key) {
        id temp = _mDict[key];
        [_mDict removeObjectForKey:key];
        [self updateData];
        return temp;
        
    }
    return nil;
}
- (id)objectForKey:(NSString *)key
{
    return _mDict[key];
}
@end
