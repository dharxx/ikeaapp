//
//  NSObject+WDIOS.h
//  DApplication
//
//  Created by Dhanu Saksrisathaporn on 12/24/2558 BE.
//  Copyright © 2558 Dhanu Saksrisathaporn. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSMutableDictionary *_mainObjects;
@interface NSObject (WDIOS)
+ (NSString *)className;
+ (instancetype)mainObject;
+ (instancetype)object;
//is not kind of class return nil
- (instancetype)cast:(Class)typeOfClass;
+ (instancetype)cast:(id)object;
@end
