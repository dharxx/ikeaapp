//
//  DataManager.h
//  IKEAApplication
//
//  Created by Dhanu Saksrisathaporn on 1/20/2559 BE.
//  Copyright © 2559 Dhanu Saksrisathaporn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataManager : NSObject

//copy new array and dict from datas
- (NSArray *)datas;
- (NSDictionary *)datasDict;
- (BOOL)hasObject:(NSString *)key;
//YES if successfully added
//NO if fail
//if already has object old one will disappear;
- (BOOL)addObject:(id)data forKey:(NSString *)key;

//return removed object
- (id)removeObject:(NSString *)key;
- (id)objectForKey:(NSString *)key;
@end
