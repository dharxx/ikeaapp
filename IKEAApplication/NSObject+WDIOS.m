//
//  NSObject+WDIOS.m
//  DApplication
//
//  Created by Dhanu Saksrisathaporn on 12/24/2558 BE.
//  Copyright © 2558 Dhanu Saksrisathaporn. All rights reserved.
//

#import "NSObject+WDIOS.h"

NSMutableDictionary *_mainObjects = nil;

@implementation NSObject (WDIOS)

- (instancetype)cast:(Class)typeOfClass
{
    return [typeOfClass cast:self];
}
+ (instancetype)cast:(id)object
{
    if ([object isKindOfClass:self]) {
        return object;
    }
    return nil;
}
+ (NSString *)className
{
    return NSStringFromClass([self class]);
}
+ (instancetype)mainObject
{
    NSString *name = [self className];
    id mainObject = nil;
    if (_mainObjects) {
        mainObject = _mainObjects[name];
    }
    else {
        _mainObjects = [NSMutableDictionary dictionary];
    }
    if (!mainObject) {
        mainObject = [[self class] object];
        _mainObjects[name] = mainObject;
    }
    return mainObject;
}

+ (instancetype)object
{
    return [[self alloc] init];
}
@end
